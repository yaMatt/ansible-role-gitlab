# GitLab Ansible Role
This is a role for Ansible to install GitLab directly on an Ubuntu machine. Enables publicly facing SSL by default.

## Variables
|          Name             |                         Description                                  | Required |        Default            |
| ------------------------- | -------------------------------------------------------------------- | -------- | ------------------------- |
| `gitlab_external_domain`  | The domain name gitlab will be presented on. i.e.: `git.example.com` |    ✔️     |   N/A                     |
| `gitlab_root_password`    | Default password for `root` account when first signing in.           |    ✔️     |   N/A                     |
| `lets_encrypt_your_email` | The e-mail address Let's Encrypt will register you with.             |    ❌    |   N/A                     |
| `gitlab_config_path`      | Path to `gitlab.rb` that configures your GitLab instance             |    ❌    | `/etc/gitlab/gitlab.rb`   |
| `gitlab_apt_source_list`  | APT source configuration to GitLab                                   |    ❌    |  `https://packages.gitlab.com/gitlab/gitlab-ce/ubuntu/ {{ ansible_lsb.codename }} main` |

## Usage
This role can be included as a role package, instructions for which you can find from the [Ansible Galaxy](https://docs.ansible.com/ansible/latest/reference_appendices/galaxy.html#installing-multiple-roles-from-a-file) pages.

You can use this role in your site playbook like so:

```yaml
- hosts: all
  remote_user: root
  tasks:
  - include_role:
      name: gitlab
    notify:
        - reconfigure gitlab
    vars:
       gitlab_external_domain: git.example.com
       gitlab_root_password: 5iveL!fe
       lets_encrypt_your_email: youremail@example.com
```
You could move the notify to another point in your playbook if you want to do extra work to GitLab config before initialising.
